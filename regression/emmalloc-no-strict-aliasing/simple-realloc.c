// SPDX-FileCopyrightText: Red Hat
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#include <stdlib.h>

int main(void) {
  char *volatile ptr = malloc(1);
  ptr = realloc(ptr, 12);
  ptr = malloc(1);

  return EXIT_SUCCESS;  // This leaks intentionally
}
