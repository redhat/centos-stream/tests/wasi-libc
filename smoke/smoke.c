// SPDX-FileCopyrightText: Red Hat
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const size_t MAX_MESSAGE_LEN = 32;
static const char *const MESSAGE = "smoke-test-ok";

int main(void) {
    char *buffer = calloc(strnlen(MESSAGE, MAX_MESSAGE_LEN), sizeof(char));
    if (!buffer) {
        return EXIT_FAILURE;
    }

    strncpy(buffer, MESSAGE, MAX_MESSAGE_LEN);
    printf("%s\n", buffer);

    free(buffer), buffer = NULL;
    return EXIT_SUCCESS;
}
