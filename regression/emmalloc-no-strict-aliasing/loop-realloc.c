// SPDX-FileCopyrightText: Red Hat
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#include <stdlib.h>

int main(void) {
  char *volatile ptr = malloc(1);
  for (unsigned i = 1; i < 20; ++i) {
    ptr = realloc(ptr, 1 << i);
  }

  free(ptr), ptr = NULL;
  return EXIT_SUCCESS;
}
